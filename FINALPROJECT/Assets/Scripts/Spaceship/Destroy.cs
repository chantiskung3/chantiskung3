﻿using UnityEngine;

public class Destroy : MonoBehaviour

{
    public GameObject Explosion;

    public GameObject PlayerExplosion;

    private GameController _gameController;
    void Start()
    {
        GameObject _gameControllerObject = GameObject.FindGameObjectWithTag("GameController");

        if (_gameControllerObject != null)
        {
            _gameController = _gameControllerObject.GetComponent<GameController>();
        }

        if (_gameControllerObject == null)
        {
            Debug.Log("Cannot find GameController Script");
        }
    }
   

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boudary")
        {
            return;
        }

        Instantiate(Explosion, transform.position, transform.rotation);
        
        

        if (other.tag == "Player")
        {
            Instantiate(PlayerExplosion, transform.position, transform.rotation);
            _gameController.gameOver();
        }

        _gameController.AddScore(1);
        Destroy(gameObject);
        Destroy(other.gameObject);
    }
}
