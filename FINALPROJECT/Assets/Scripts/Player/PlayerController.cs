﻿using System;
using UnityEngine;

[Serializable]
public class Boundary
{
    public float xMin, Xmax, zMin, Zmax;
}
public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    
    public float speed;

    public Boundary _boundary;

    public float titl;

    public GameObject shot;

    public Transform shotSpawn;

    public float fireRate;
    private float nextFire;
    
   
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }


    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");

        float moveVertical = Input.GetAxis("Vertical");


        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.velocity = movement * speed;

        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, _boundary.xMin, _boundary.Xmax),
            0.0f,
            Mathf.Clamp(rb.position.z, _boundary.zMin, _boundary.Zmax)
        );
        
        rb.rotation = Quaternion.Euler(0.0f,0.0f,rb.velocity.x*-titl);
    }

    void Update()
    {
        if (Input.GetButton("Fire1")&& Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            GetComponent<AudioSource>().Play();
        }
    }
    
}
