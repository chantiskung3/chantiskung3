﻿using UnityEngine;

public class EnemyBoltController : MonoBehaviour

{

    public GameObject PlayerExplosion;

    private GameController _gameController;
    void Start()
    {
        GameObject _gameControllerObject = GameObject.FindGameObjectWithTag("GameController");

        if (_gameControllerObject != null)
        {
            _gameController = _gameControllerObject.GetComponent<GameController>();
        }

        if (_gameControllerObject == null)
        {
            Debug.Log("Cannot find GameController Script");
        }
    }
   

    private void OnTriggerEnter(Collider other)
    {

       
        
        

        if (other.tag == "Player")
        {
            Instantiate(PlayerExplosion, transform.position, transform.rotation);
            _gameController.gameOver();
        }

        
        Destroy(gameObject);
        Destroy(other.gameObject);
    }
}