﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed;
    private GameObject player;
    private Rigidbody2D rb;
   
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (player != null)
        {
            float moveX = 0f;
            
            if (Mathf.Round(player.transform.position.x) > Mathf.Round(transform.position.x))
            {
                moveX = 1f;
            }

            if (Mathf.Round(player.transform.position.x) < Mathf.Round(transform.position.x))
            {
                moveX = -1f;
            }
            rb.velocity = new Vector3(moveX,0f,-1f);
            
        }
    }
}
