﻿using System;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public int hazardCount;
    public float spawnWaite;
    public Vector3 spawnValues;
    public float waveWaite;
    public float startWaite;

    public Text ScoreText;
    public Text GameOverText;
    public Text RestartText;

    private int Score;

    private Boolean GameOver;
    private Boolean Restart;
    
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        GameOver = false;
        Restart = false;

        GameOverText.text = "";
        RestartText.text = "";

        Score = 0;
        UpdateScore();
        
        
        StartCoroutine(SpawnWaves());
    }

    public void AddScore(int newScoreValue)
    {
        Score += newScoreValue;
        UpdateScore();
    }
     void UpdateScore()
     {
         ScoreText.text = "Score : " + Score.ToString();
     }

    public void gameOver()
    {
        GameOver = true;
        GameOverText.text = "Game Over";
    }
    
     void Update()
    {
        if (GameOver )
        {
            Restart = true;
            RestartText.text = "Press R for Restart";
        }

        if (Restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
            
        }
        
        
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWaite);

        
        {
            for (int i = 0; i < hazardCount; i++)
            {


                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y,
                    spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);

                yield return new WaitForSeconds(spawnWaite);
            }
            yield return new WaitForSeconds(waveWaite);
        }
    }

    private void FixedUpdate()
    {
        if ( Input.GetKeyDown(KeyCode.F))
        {
            SceneManager.LoadScene("Game2");
        }
    }
}
